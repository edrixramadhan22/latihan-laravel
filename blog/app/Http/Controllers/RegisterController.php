<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function form() {
        return view('form');
    }

    public function loged() {
        return view('loged');
    }

    public function submit(Request $request) {
        $firstname = $request['fname'];
        $middlename = $request['mname'];
        $lastname = $request['lname'];
        return view('loged', compact('firstname','middlename','lastname'));
    }
}
